<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>

<title>消費カロリー計算機</title>
<script>
<!--
tee = 0;
tea = 0;

function all_clear(){
var w = 0;
var tm = 0;
tee = 0;
tea = 0;
document.excalc.details.value = "";
document.excalc.ee.value = "";
document.excalc.ea.value = "";
document.excalc.u1.focus();
}

function top_focus(){
document.excalc.w.focus();
}

function calc1(){
if (document.excalc.w.value==""){
alert("体重を入力してください");
document.excalc.w.focus();
return fales;
}
if (document.excalc.u1.value==""){
alert("身体活動の種類を選んでください");
document.excalc.u1.focus();
return fales;
}
if (document.excalc.tm1.value==""){
alert("活動時間を入力してください");
document.excalc.tm1.focus();
return fales;
}

var wt = Number(document.excalc.w.value);
if (isNaN(wt)){
alert("体重が間違っています．入力し直してください！");
document.excalc.w.focus();
return fales;
} else if (wt<=0){
alert("体重が間違っています．入力し直してください！");
document.excalc.w.focus();
return fales;
}

var tm = Number(document.excalc.tm1.value);
if (isNaN(tm)){
alert("活動時間が間違っています．入力し直してください！");
document.excalc.tm1.focus();
return fales;
}

var kind = document.excalc.u1.value.substring(7);
var met = parseFloat(document.excalc.u1.value.substring(2,6));
    //消費カロリー計算
var ee = Math.floor((1.05 * met * tm / 60 * wt) * 10)/10;
    //安静時代謝を除いた計算
var ea = Math.floor((1.05 * (met - 1) * tm / 60 * wt) * 10)/10;

var metc = String(met);
if (metc.indexOf(".") == -1){
metc = metc + ".0";
}
var rslt = ":" + metc + "METs･" + tm +"min. " + "=" + ee + "kcal";

tee = Math.floor((tee + ee)*10)/10;
tea = Math.floor((tea + ea)*10)/10;

document.excalc.details.value = document.excalc.details.value + kind + rslt + "\n";
document.excalc.ee.value = String(tee);
document.excalc.ea.value = String(tea);

}
// -->
</script>
<link rel="stylesheet" href="css/kalcal.css" type="text/css">
<style type="text/css">

input{
  font-size : 16pt;
  text-align : right;
}
select{
  font-family : "ＭＳ ゴシック";
}

</style>
</head>
 <header>
<jsp:include page="/baselayout/header.jsp" />
</header>


<h1 class="cal">消費カロリー計算機</h1>
<table border="0" width="95%" align="center">


<form name="excalc">
<table border="0" width="95%" align="center">
  <tbody>
    <tr>
      <td class="Sz16Lh18">　<b>体重</b></td>
      <td class="Sz11" nowrap><input size="3" type="text" name="w" value="${userInfo.weight}">　kg</td>
    </tr>
    <tr>
      <td nowrap class="Sz12Lh18">　<b>身体活動の種類</b></td>
      <td nowrap><select name="u1" style="font-size : small;">
        <OPTION>■歩行・走行--------------------------------------------------</OPTION>
        <OPTION value="0,02.5,子どもと歩く" selected>子どもと歩く</OPTION>
        <OPTION value="0,02.0,ゆっくりした歩行（平地：散歩または家の中，非常に遅い=54m/分未満）">ゆっくりした歩行（平地：散歩または家の中，非常に遅い=54m/分未満）</OPTION>
        <OPTION value="0,02.5,ゆっくりした歩行（平地：遅い=54m/分）">ゆっくりした歩行（平地：遅い=54m/分）</OPTION>
        <OPTION value="0,03.0,歩行（平地：67m/分，幼い子ども・犬を連れて，買い物など）">歩行（平地：67m/分，幼い子ども・犬を連れて，買い物など）</OPTION>
        <OPTION value="0,03.3,歩行（平地：81m/分，通勤時など）">歩行（平地：81m/分，通勤時など）</OPTION>
        <OPTION value="1,03.8,速歩（平地：やや速め 94m/分程度）">速歩（平地：やや速め 94m/分程度）</OPTION>
        <OPTION value="1,04.0,速歩（平地：95～100m/分程度）">速歩（平地：95～100m/分程度）</OPTION>
        <OPTION value="1,05.0,速歩（平地：速く107m/分程度）">速歩（平地：速く107m/分程度）</OPTION>
        <OPTION value="1,06.0,ジョギングと歩行の組み合わせ（ジョギングは10分以下）">ジョギングと歩行の組み合わせ（ジョギングは10分以下）</OPTION>
        <OPTION value="1,07.0,ジョギング">ジョギング</OPTION>
        <OPTION value="1,08.0,ランニング（134m/分）">ランニング（134m/分）</OPTION>
        <OPTION value="1,10.0,ランニング（161m/分）">ランニング（161m/分）</OPTION>
        <OPTION value="1,15.0,ランニング（階段を上がる）">ランニング（階段を上がる）</OPTION>
        <OPTION value="0,08.0,階段を上がる">階段を上がる</OPTION>
        <OPTION value="0,03.0,階段を下りる">階段を下りる</OPTION>
        <OPTION>■自転車・車--------------------------------------------------</OPTION>
        <OPTION value="1,03.0,自転車エルゴメーター（50ワット），とても軽い">自転車エルゴメーター（50ワット），とても軽い</OPTION>
        <OPTION value="1,05.5,自転車エルゴメーター（100ワット），軽い">自転車エルゴメーター（100ワット），軽い</OPTION>
        <OPTION value="0,04.0,自転車に乗る（16km/時未満）">自転車に乗る（16km/時未満）</OPTION>
        <OPTION value="1,08.0,サイクリング（約20km/時）">サイクリング（約20km/時）</OPTION>
        <OPTION value="0,02.5,スクーターに乗る">スクーターに乗る</OPTION>
        <OPTION value="0,02.5,オートバイに乗る">オートバイに乗る</OPTION>
        <OPTION value="0,01.0,車に乗る">車に乗る</OPTION>
        <OPTION value="0,01.5,車の運転">車の運転</OPTION>
        <OPTION>■体操・バレエ・筋トレ----------------------------------------</OPTION>
        <OPTION value="1,02.5,ストレッチング">ストレッチング</OPTION>
        <OPTION value="1,03.5,体操（家で，軽・中強度）">体操（家で，軽・中強度）</OPTION>
        <OPTION value="1,02.5,ヨガ">ヨガ</OPTION>
        <OPTION value="1,04.0,太極拳">太極拳</OPTION>
        <OPTION value="1,04.8,バレエ（モダン，ツイスト，ジャズ，タップ）">バレエ（モダン，ツイスト，ジャズ，タップ）</OPTION>
        <OPTION value="1,06.0,美容体操">美容体操</OPTION>
        <OPTION value="1,06.0,ジャズダンス">ジャズダンス</OPTION>
        <OPTION value="1,06.5,エアロビクス">エアロビクス</OPTION>
        <OPTION value="1,03.0,ウェイトトレーニング（軽・中強度）">ウェイトトレーニング（軽・中強度）</OPTION>
        <OPTION value="1,06.0,ウェイトトレーニング（高強度）">ウェイトトレーニング（高強度）</OPTION>
        <OPTION value="1,06.0,ウェイトトレーニング（パワーリフティング）">ウェイトトレーニング（パワーリフティング）</OPTION>
        <OPTION value="1,06.0,ウェイトトレーニング（ボディビル）">ウェイトトレーニング（ボディビル）</OPTION>
        <OPTION>■水中運動・水泳----------------------------------------------</OPTION>
        <OPTION value="1,04.0,水中運動">水中運動</OPTION>
        <OPTION value="1,04.0,水中で柔軟体操">水中で柔軟体操</OPTION>
        <OPTION value="1,04.0,水中体操">水中体操</OPTION>
        <OPTION value="1,04.0,アクアビクス">アクアビクス</OPTION>
        <OPTION value="1,06.0,スイミング（ゆっくりしたストローク）">スイミング（ゆっくりしたストローク）</OPTION>
        <OPTION value="1,10.0,水泳（平泳ぎ）">水泳（平泳ぎ）</OPTION>
        <OPTION value="1,08.0,水泳（クロール：ゆっくり（約45m/分），軽・中強度）">水泳（クロール：ゆっくり（約45m/分），軽・中強度）</OPTION>
        <OPTION value="1,11.0,水泳（クロール：速い（約70m/分），活発）">水泳（クロール：速い（約70m/分），活発）</OPTION>
        <OPTION value="1,07.0,水泳（背泳）">水泳（背泳）</OPTION>
        <OPTION value="1,11.0,水泳（バタフライ）">水泳（バタフライ）</OPTION>
        <OPTION>■家事・仕事・作業--------------------------------------------</OPTION>
        <OPTION value="0,04.0,通勤">通勤</OPTION>
        <OPTION value="0,01.5,軽いオフィスワーク">軽いオフィスワーク</OPTION>
        <OPTION value="0,01.5,タイプ">タイプ</OPTION>
        <OPTION value="0,02.3,コピー（立位）">コピー（立位）</OPTION>
        <OPTION value="0,02.3,立ち仕事（店員，工場など）">立ち仕事（店員，工場など）</OPTION>
        <OPTION value="0,02.0,料理や食材の準備（立位，座位）">料理や食材の準備（立位，座位）</OPTION>
        <OPTION value="0,02.5,料理や食材の準備・片付け（歩行）">料理や食材の準備・片付け（歩行）</OPTION>
        <OPTION value="0,02.5,盛り付け">盛り付け</OPTION>
        <OPTION value="0,02.5,テーブルセッティング">テーブルセッティング</OPTION>
        <OPTION value="0,02.3,皿洗い（立位）">皿洗い（立位）</OPTION>
        <OPTION value="0,02.0,洗濯物を洗う，しまう">洗濯物を洗う，しまう</OPTION>
        <OPTION value="0,02.3,服・洗濯物の片付け">服・洗濯物の片付け</OPTION>
        <OPTION value="0,02.3,アイロンがけ">アイロンがけ</OPTION>
        <OPTION value="0,02.5,掃除：軽い（ごみ掃除，整頓，リネンの交換，ごみ捨て）">掃除：軽い（ごみ掃除，整頓，リネンの交換，ごみ捨て）</OPTION>
        <OPTION value="0,03.0,屋内の掃除">屋内の掃除</OPTION>
        <OPTION value="0,03.3,カーペット掃き">カーペット掃き</OPTION>
        <OPTION value="0,03.3,フロア掃き">フロア掃き</OPTION>
        <OPTION value="0,03.5,モップかけ">モップかけ</OPTION>
        <OPTION value="0,03.5,掃除機で掃除">掃除機で掃除</OPTION>
        <OPTION value="0,03.8,床磨き">床磨き</OPTION>
        <OPTION value="0,03.8,風呂掃除">風呂掃除</OPTION>
        <OPTION value="0,04.5,庭の草むしり">庭の草むしり</OPTION>
        <OPTION value="0,05.5,芝刈り（電動芝刈り機を使って，歩きながら）">芝刈り（電動芝刈り機を使って，歩きながら）</OPTION>
        <OPTION value="0,04.0,屋根の雪下ろし">屋根の雪下ろし</OPTION>
        <OPTION value="0,06.0,スコップで雪かき">スコップで雪かき</OPTION>
        <OPTION value="0,03.0,大工仕事">大工仕事</OPTION>
        <OPTION value="0,02.0,荷作り（立位）">荷作り（立位）</OPTION>
        <OPTION value="0,03.0,梱包作業">梱包作業</OPTION>
        <OPTION value="0,03.5,箱詰め作業">箱詰め作業</OPTION>
        <OPTION value="0,03.5,軽い荷物運び">軽い荷物運び</OPTION>
        <OPTION value="0,03.0,車の荷物の積み下ろし">車の荷物の積み下ろし</OPTION>
        <OPTION value="0,03.0,家財道具の片付け">家財道具の片付け</OPTION>
        <OPTION value="0,06.0,家具・家財道具の移動・運搬">家具・家財道具の移動・運搬</OPTION>
        <OPTION value="0,08.0,運搬（重い負荷）">運搬（重い負荷）</OPTION>
        <OPTION value="0,09.0,荷物を運ぶ（上の階へ運ぶ）">荷物を運ぶ（上の階へ運ぶ）</OPTION>
        <OPTION value="0,04.5,耕作">耕作</OPTION>
        <OPTION value="0,02.5,農作業（収穫機の運転，干し草の刈り取り，灌概の仕事）">農作業（収穫機の運転，干し草の刈り取り，灌概の仕事）</OPTION>
        <OPTION value="0,04.5,農作業（家畜に餌を与える）">農作業（家畜に餌を与える）</OPTION>
        <OPTION value="0,08.0,農作業（干し草をまとめる）">農作業（干し草をまとめる）</OPTION>
        <OPTION value="0,08.0,農作業（納屋の掃除，鶏の世話），活発">農作業（納屋の掃除，鶏の世話），活発</OPTION>
        <OPTION value="0,03.5,電気関係の仕事（配管工事）">電気関係の仕事（配管工事）</OPTION>
        <OPTION value="0,04.0,高齢者や障害者の介護">高齢者や障害者の介護</OPTION>
        <OPTION value="0,04.0,車椅子を押す">車椅子を押す</OPTION>
        <OPTION>■趣味・娯楽・競技スポーツ------------------------------------</OPTION>
        <OPTION value="0,01.0,静かに座って（あるいは寝転がって）テレビ・音楽鑑賞">静かに座って（あるいは寝転がって）テレビ・音楽鑑賞</OPTION>
        <OPTION value="0,01.5,編み物・手芸（座位）">編み物・手芸（座位）</OPTION>
        <OPTION value="0,01.8,手芸（立位）">手芸（立位）</OPTION>
        <OPTION value="1,03.5,ゴルフ（カートを使って，待ち時間を除く）">ゴルフ（カートを使って，待ち時間を除く）</OPTION>
        <OPTION value="1,04.5,ゴルフ（クラブを自分で運ぶ，待ち時間を除く）">ゴルフ（クラブを自分で運ぶ，待ち時間を除く）</OPTION>
        <OPTION value="0,03.0,釣り（全般）">釣り（全般）</OPTION>
        <OPTION value="0,02.5,釣り（船で座って）">釣り（船で座って）</OPTION>
        <OPTION value="0,06.0,釣り（渓流フィッシング）">釣り（渓流フィッシング）</OPTION>
        <OPTION value="0,04.0,レジャー・娯楽">レジャー・娯楽</OPTION>
        <OPTION value="0,02.5,植物への水やり">植物への水やり</OPTION>
        <OPTION value="0,04.5,苗木の植栽">苗木の植栽</OPTION>
        <OPTION value="0,02.0,ギター演奏（クラシックやフォーク（座位））">ギター演奏（クラシックやフォーク（座位））</OPTION>
        <OPTION value="0,03.0,ギター演奏（立ってロック）">ギター演奏（立ってロック）</OPTION>
        <OPTION value="0,02.5,ピアノ演奏">ピアノ演奏</OPTION>
        <OPTION value="0,02.5,オルガン演奏">オルガン演奏</OPTION>
        <OPTION value="0,04.0,ドラム演奏">ドラム演奏</OPTION>
        <OPTION value="0,02.3,カジノ・ギャンブル">カジノ・ギャンブル</OPTION>
        <OPTION value="1,03.0,ボーリング">ボーリング</OPTION>
        <OPTION value="1,03.0,フリスビー">フリスビー</OPTION>
        <OPTION value="1,04.0,卓球">卓球</OPTION>
        <OPTION value="1,04.5,バドミントン">バドミントン</OPTION>
        <OPTION value="1,07.0,テニス">テニス</OPTION>
        <OPTION value="1,02.5,キャッチボール（フットボール，野球）">キャッチボール（フットボール，野球）</OPTION>
        <OPTION value="1,05.0,ソフトボール">ソフトボール</OPTION>
        <OPTION value="1,05.0,野球">野球</OPTION>
        <OPTION value="1,03.0,バレーボール">バレーボール</OPTION>
        <OPTION value="1,06.0,バスケットボール">バスケットボール</OPTION>
        <OPTION value="1,07.0,サッカー">サッカー</OPTION>
        <OPTION value="1,10.0,ラグビー">ラグビー</OPTION>
        <OPTION value="1,07.0,スキー">スキー</OPTION>
        <OPTION value="1,07.0,スケート">スケート</OPTION>
        <OPTION value="1,07.5,山を登る（約1～2kgの荷物を背負って）">山を登る（約1～2kgの荷物を背負って）</OPTION>
        <OPTION value="1,05.0,石蹴り">石蹴り</OPTION>
        <OPTION value="1,05.0,ドッジボール">ドッジボール</OPTION>
        <OPTION value="1,05.0,遊戯具">遊戯具</OPTION>
        <OPTION value="1,05.0,ビー玉遊び">ビー玉遊び</OPTION>
        <OPTION>■格闘技------------------------------------------------------</OPTION>
        <OPTION value="1,10.0,柔道">柔道</OPTION>
        <OPTION value="1.10.0,柔術">柔術</OPTION>
        <OPTION value="1,10.0,空手">空手</OPTION>
        <OPTION value="1,10.0,キックボクシング">キックボクシング</OPTION>
        <OPTION value="1,10.0,テコンドー">テコンドー</OPTION>
        <OPTION>■その他生活活動----------------------------------------------</OPTION>
        <OPTION value="0,01.0,リクライニング">リクライニング</OPTION>
        <OPTION value="0,01.2,静かに立つ">静かに立つ</OPTION>
        <OPTION value="0,01.3,新聞等を読む（座位）">新聞等を読む（座位）</OPTION>
        <OPTION value="0,01.5,会話，電話，読書（座位）">会話，電話，読書（座位）</OPTION>
        <OPTION value="0,01.8,会話，電話，読書（座位）">会話，電話，読書（立位）</OPTION>
        <OPTION value="0,01.5,食事（座位）">食事（座位）</OPTION>
        <OPTION value="0,02.0,食事（立位）">食事（立位）</OPTION>
        <OPTION value="0,02.0,会話をしながら食事をする">会話をしながら食事をする</OPTION>
        <OPTION value="0,02.0,着替え">着替え</OPTION>
        <OPTION value="0,02.0,身の回り（歯磨き，手洗い，髭剃りなど）">身の回り（歯磨き，手洗い，髭剃りなど）</OPTION>
        <OPTION value="0,02.0,シャワーを浴びる">シャワーを浴びる</OPTION>
        <OPTION value="0,02.0,タオルで拭く（立位）">タオルで拭く（立位）</OPTION>
        <OPTION value="0,01.5,入浴（座位）">入浴（座位）</OPTION>
        <OPTION value="0,01.5,動物の世話（座位）">動物の世話（座位）</OPTION>
        <OPTION value="0,02.8,動物の世話（軽度）">動物の世話（軽度）</OPTION>
        <OPTION value="0,02.5,子ども・動物の世話">子ども・動物の世話</OPTION>
        <OPTION value="0,03.0,子どもの世話（立位）">子どもの世話（立位）</OPTION>
        <OPTION value="0,02.5,子どもを乗せたベビーカーを押す">子どもを乗せたベビーカーを押す</OPTION>
        <OPTION value="0,02.5,子どもと遊ぶ（座位，軽い）">子どもと遊ぶ（座位，軽い）</OPTION>
        <OPTION value="0,02.8,子どもと遊ぶ（立位，軽度）">子どもと遊ぶ（立位，軽度）</OPTION>
        <OPTION value="0,04.0,子どもと遊ぶ（歩く/走る，中強度）">子どもと遊ぶ（歩く/走る，中強度）</OPTION>
        <OPTION value="0,04.0,子どもと遊ぶ・動物の世話（徒歩/走る，中強度）">子どもと遊ぶ・動物の世話（徒歩/走る，中強度）</OPTION>
        <OPTION value="0,05.0,子どもと遊ぶ・動物の世話（歩く/走る，活発に）">子どもと遊ぶ・動物の世話（歩く/走る，活発に）</OPTION>
        <OPTION value="1,05.0,子どもの遊び（全般）">子どもの遊び（全般）</OPTION>
      </select></td>
    </tr>
    <tr>
      <td class="Sz16Lh18" nowrap>　<b>活動時間</b>　</td>
      <td class="Sz11" nowrap><input size="3" type="text" name="tm1">　分　　（体重，活動時間は<font color="red">半角数字</font>で入力）</td>
    </tr>
  </tbody>
</table>
           <input type="button" name="b1" value="計算　" onclick="calc1()" style="font-size : 10pt;" class="cal1">
<hr>
<table border="0" width="95%" align="center">
  <tbody>
    <tr>
      <td class="explntn">計算結果一覧</td>
    </tr>
    <tr>
      <td><textarea rows="10" cols="50" readonly name="details"></textarea></td>
    </tr>
    <tr>
      <td valign="bottom"><b>消費カロリー合計</b> <input size="6" type="text" name="ee" readonly style="font-weight : bold;"> kcal </td>
    </tr>
    <tr>
      <td><input type="button" name="b1" value="クリア" onclick="all_clear()" style="font-size : 10pt;"></td>
    </tr>
  </tbody>
</table>
</form>

</body>
</html>