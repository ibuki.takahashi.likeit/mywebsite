<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>

</head>
<link rel="stylesheet" href="css/login.css">


<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<section class="forms-section">
  <h1 class="section-title">ログイン</h1>
  <div class="forms">
    <div class="form-wrapper is-active">
      <button type="button" class="switcher switcher-login">
        Login
        <span class="underline"></span>
      </button>
      <form class="form form-login" action="Login" method="post">
        <fieldset>
          <legend>Please, enter your ID and password for login.</legend>
          <div class="input-block">
            <label for="login-id">ID</label>
            <input id="login-id" type="text" name="loginId" required>
          </div>
          <div class="input-block">
            <label for="login-password">Password</label>
            <input id="login-password" type="password" name="password" required>
          </div>
        </fieldset>
        <button type="submit" class="btn-login">Login</button>
      </form>
    </div>
    <div class="form-wrapper">
      <button type="button" class="switcher switcher-signup">
        Sign Up
        <span class="underline"></span>
      </button>
      <form class="form form-signup" action="SignUp" method="post">
        <fieldset>
          <legend>Please, enter your ID, password and password confirmation for sign up.</legend>
          <div class="input-block">
            <label for="login-id">ID</label>
            <input id="login-id" type="text" name="loginId" required>
          </div>
            <div class="input-block">
            <label for="user-name">User Name</label>
            <input id="user-name" type="text" name="name" required>
          </div>
          <div class="input-block">
            <label for="signup-password">Password</label>
            <input id="signup-password" type="password" name="password" required>
          </div>
          <div class="input-block">
            <label for="signup-password-confirm">Confirm password</label>
            <input id="signup-password-confirm" type="password" name="password2" required>
          </div>
        </fieldset>
        <button type="submit" class="btn-signup">Continue</button>
      </form>
    </div>
  </div>

    <script type="text/javascript" src="js/login.js"></script>
</section>