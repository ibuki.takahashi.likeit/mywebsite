<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>アカウント消去</title>

</head>
<header>
	<jsp:include page="/baselayout/header.jsp" />
</header>
<link rel="stylesheet" href="css/delete.css">

<h1>アカウント削除</h1>
<div class="btn">
	<div class="btn-back">
		<p>Are you sure you want to do that?</p>
		<p class="delete">※消去したデータは戻りません</p>
		<form class="form-delete" action="Delete" method="post">
			<button class="yes" name="answer" value="0">Yes</button>
			<button class="no" name="answer" value="1">No</button>
		</form>
	</div>
	<div class="btn-front">Delete</div>
</div>


<script type="text/javascript" src="js/delete.js"></script>
</html>