<html>
<head>
<meta charset="UTF-8">
<title>早見表EXERCISE</title>

</head>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<meta charset="UTF-8">
<title>detail 名</title>
<meta name="description" content="サイトの説明文">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="canonical" href="あなたのサイトURL">
<link rel="icon" type="image/png" href="ファビコンのパス" />
<!-- OGP設定 -->
<meta property="og:type" content="website" />
<meta property="og:url" content="あなたのサイトURL" />
<meta property="og:image" content="SNSで表示させたい画像のパス" />
<meta property="og:title" content="ページタイトル" />
<meta property="og:description" content="サイトの説明文" />

<!-- スタイルシートはここから -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
<link rel="stylesheet" href="serchdetailburn.css">
</head>
<body>

    <header class="flex-grow-1 mb-3">
        
    </header>
    
    <!--=============================================================================== -->
    <main class="w-100">
        <div class="container-fluid"> 
            
            <section>
                <div class="row"> 
                    <!------ 商品一覧　１品目 ----->
                    <div class="col-md-4 pr-md-0 mb-4">
                        <div class="card shadow-sm h-100">
                            <div class="position-relative"><a href="#"><img src="Downloads/shop_001_DL/img/top_p04.jpg" alt="" class="card-img-top"></a>
                                
                            </div>
                            <div class="card-body">
                                <h3 class="mb-3 font-weight-bold">Food名</h3>
                                <h4 class="font-weight-bold">???  <span class="h6">（kcal）</span></h4>
                                <p class="card-text mb-3">天ぷら（てんぷら・天麩羅・天婦羅）は、魚介類や野菜等の食材を小麦粉を主体とした衣で包み、油で揚げて調理する日本料理である。</p>
                              
                            </div>
                        </div>
                    </div>
                    <!------ 商品一覧　２品目 ----->
                    <div class="col-lg-8">
                        <div class="card flex-md-row shadow-sm mb-4">
                            <div class="card-img"><img src="img/rice-balls-2031330-26-5b3fd34346e0fb00376633fa.jpg" alt="" class="w-100"></div>
                            <div class="card-body d-flex flex-column align-items-start">
                               
                                <h3 class="mb-3 font-weight-bold">Food名</h3>
                                <h4 class="font-weight-bold">??? <span class="h6">（kcal）</span></h4>
                                <p class="card-text mb-3">おにぎり（御握り）は、ご飯を三角形・俵形・球状などに加圧成型した食べ物である。通常は掌（てのひら）に載る程度の大きさに作る。「にぎり」「握り飯（にぎりめし）」「握飯（にぎりめし、にぎりいい）」、「むすび」「おむすび（御結び）」「おつくね」などともいう。</p>
                              
                            </div>
                        </div>
                    </div>
                    <!--=============================================================================== --> 
                </div>
                <!--/.row--> 
                <!--=============================================================================== -->
                <div class="row"> 
                    <!------ 商品一覧　３品目 ----->
                    <div class="col-md-8 pr-md-0">
                        <div class="card flex-md-row shadow-sm mb-4">
                            <div class="card-img"><img src="img/091616_best_cheap_burgers_slide_0_fs.max-784x410.jpg" alt="" class="w-100"></div>
                            <div class="card-body d-flex flex-column align-items-start">
                                
                                <h3 class="mb-3 font-weight-bold">Food名</h3>
                                <h4 class="font-weight-bold">???  <span class="h6">（kcal）</span></h4>
                                <p class="card-text mb-3">？？？？？？？？？？？</p>
                               
                            </div>
                        </div>
                        <div class="row"> 
                            <!------ 商品一覧　４品目 ----->
                            <div class="col-md-6">
                                <div class="card shadow-sm mb-4">
                                    <div class="position-relative"><a href="#"><img src="img/1200px-Shoyu_ramen,_at_Kasukabe_Station_(2014.05.05)_2.jpg" alt="" class="card-img-top"></a>
                                       
                                    </div>
                                    <div class="card-body">
                                        <h3 class="mb-3 font-weight-bold">Food名</h3>
                                        <h4 class="font-weight-bold">???  <span class="h6">（kcal）</span></h4>
                                        <p class="card-text mb-3">？？？？？？？？？？？？</p>
                                       
                                    </div>
                                </div>
                            </div>
                            <!------ 商品一覧　５品目 ----->
                            <div class="col-md-6">
                                <div class="card shadow-sm mb-4">
                                    <div class="position-relative"><a href="#"><img src="img/photo_Gyudon-Beef.jpeg" alt="" class="card-img-top"></a>
                                        
                                    </div>
                                    <div class="card-body">
                                        <h3 class="mb-3 font-weight-bold">Food名</h3>
                                        <h4 class="font-weight-bold">???  <span class="h6">（kcal）</span></h4>
                                        <p class="card-text mb-3">？？？？？？？？？？？？</p>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!------ 商品一覧　６品目 ----->
                    <div class="col-md-4 mb-4">
                        <div class="card shadow-sm mb-4 h-100">
                            <div class="card-img"><img src="img/0817-murray-mancini-dried-tomato-pie.jpg" alt="" class="w-100"></div>
                            <div class="card-body d-flex flex-column align-items-start">
                               
                                <h3 class="mb-3 font-weight-bold">Food名</h3>
                                <h4 class="font-weight-bold">??? <span class="h6">（kcal）</span></h4>
                                <p class="card-text mb-3">？？？？？？？？？？？？？？</p>
                               
                            </div>
                        </div>
                    </div>
                </div>
              
           
</body>
</html>