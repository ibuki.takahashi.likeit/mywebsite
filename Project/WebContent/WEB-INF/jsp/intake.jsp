<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>カロリー追加</title>

</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/intake.css">
 <header>
<jsp:include page="/baselayout/header.jsp" />
</header>
<form class="intakedate" action="Intake" method="post">
  <h1><i class="fa fa-cutlery" aria-hidden="true"></i> Intake</h1>
  <input type="text" name="foodName" placeholder="Food name">
  <input type="text" name="foodEnergy" placeholder="Kcal">

  <button type="submit">Add</button>
</form>

<a href="UserHome" class="return"><i class="fa fa-undo" aria-hidden="true"></i></a>