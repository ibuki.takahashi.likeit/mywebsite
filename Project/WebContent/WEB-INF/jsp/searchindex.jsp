<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>ホーム</title>

</head>
<link rel="stylesheet" href="css/historyindex.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<div id="header">
      <header>
<jsp:include page="/baselayout/header.jsp" />
</header>
    <div class="center">
    <div class="middle">
      <h1>Exercise calculator</h1>
        <div class="btn-container">
            <h5>運動の簡易計算ができます。</h5>
  <a href="KalCal" class="btn">Calculator :)</a>
</div>
        <div class="btn-container2">
            <h5>食事のカロリー早見表になります。</h5>
  <a href="FoodSample" class="btn">Food chart :)</a>
</div>
    </div>
  </div>
</div>