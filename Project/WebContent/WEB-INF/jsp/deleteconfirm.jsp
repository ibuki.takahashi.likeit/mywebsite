<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>消去確認</title>

</head>
<link rel="stylesheet" href="css/deleteconfirm.css">
<div class="blackboard question">
<p>あなたのアカウントはさくじょされました。</p>
    <p>ログインがめんにもどりますか？</p>
</div>

<div class="clearfix">
<div class="blackboard1 answer float-r">

    <a href="Login" class="return"><p class="blink-before">はい</p></a>
<p>いいえ</p>
</div>
</div>
