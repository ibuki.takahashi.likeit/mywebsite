<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>Edit</title>
<link rel="stylesheet" href="css/editib.css">
</head>
 <header>
<jsp:include page="/baselayout/header.jsp" />
</header>
<form  action="EditIB" method="post">
<h1>Edit your intake$exercise</h1>
<h5 style="text-align:center">本日分の追加要素になります。</h5>
<h5 style="text-align:center">※日を跨ぐと本日分の追加要素は変更できなくなります。</h5>
<div id="box" class="box blurred-bg tinted">
  <div class="content">
    <h1>Your intake</h1>
<table width="777" height="80">
			<tr><th width="30%"></th><th width="30%">Food</th><th width="30%">Kcal</th></tr>

<c:forEach var="FL" items="${FoodList}" >
<tr>
<td align="center"><button type="submit" name="intake_date" value="${FL.id}"><font color="blue">★</font></button>
</td>
<td align="center">${FL.intakeName}</td><td align="center">${FL.intakeEnergy}</td></tr></c:forEach>

			</table>
    </div>
    </div>
 <div id="box" class="box1 blurred-bg tinted">
  <div class="content">
    <h1>Your Exercise</h1>

      <table width="777" height="80">
	<tr><th width="30%"></th><th width="30%">Exercise</th><th width="30%">Kcal</th></tr>
<c:forEach var="EL" items="${ExList}">
<tr>
<td align="center"><button type="submit" name="burn_date" value="${EL.id}"><font color="blue">★</font></button>
</td>
<td align="center">${EL.burnName}</td><td align="center">${EL.burnEnergy}</td>
</tr>
</c:forEach>
</table>

</div>
    </div>
	</form>