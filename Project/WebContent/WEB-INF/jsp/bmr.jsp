<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="UTF-8">
<title>ホーム</title>

</head>
<link rel="stylesheet" href="css/bmr.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<div id="header">
 <header>
<jsp:include page="/baselayout/header.jsp" />
</header>



  <div class="center" id="headerc">
    <div class="middle">
      <h1>${userInfo.oneDayEnergy}kcal</h1>
      <h2>あなたの1日分のカロリー</h2>
      <h5>※1日に必要なカロリーなのでマイナスからスタートになります。</h5>
    </div>
  </div>
</div>

<div id="pageHr">
  <i><i>⇓</i></i>
</div>

<div id="page">
    <tr>
        <pre> </pre>
        <pre> </pre>
        <pre> </pre>
        <pre> </pre>
        <pre> </pre>
        <pre> </pre>
        <pre> </pre>
        <td><p><font color="white"><i class="fa fa-bicycle" aria-hidden="true"></i>カロリー消費</font></p></td>
        <td><p><font color="white"><i class="fa fa-cutlery" aria-hidden="true"></i>カロリー追加</font></p></td>
        <td><p><font color="white"><i class="fa fa-file-text-o" aria-hidden="true"></i>カロリー早見表</font></p></td>
        <td><p><font color="white"><i class="fa fa-history" aria-hidden="true"></i>履歴</font></p></td>
    </tr>


<button id="menuButton" class="button"><i class="fa fa-bars flip" aria-hidden="true"></i></button>
<a href="Burn" class="burn"><button id="fit" class="otherButton1 hidden openMenu"><i class="fa fa-bicycle" aria-hidden="true"></i></button></a>
<a href="Intake" class="intake"><button id="eat" class="otherButton2 hidden openMenu"><i class="fa fa-cutlery" aria-hidden="true"></i></button></a>
<a href="HistoryIndex" class="History"><button id="history" class="otherButton3 hidden openMenu"><i class="fa fa-history" aria-hidden="true"></i></button></a>
<a href="SearchIndex" class="Calculator"><button id="sampletext" class="otherButton4 hidden openMenu"><i class="fa fa-file-text-o" aria-hidden="true"></i></button></a>



<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>




</div>
 <script type="text/javascript" src="js/bmr.js"></script>
