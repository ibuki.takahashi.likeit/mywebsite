package EC;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/UserHome")
public class UserHome extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserHome() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user1 =(User) session.getAttribute("userInfo");
		if(user1==null) {
			response.sendRedirect("Login");
			return;
		}
		  request.setCharacterEncoding("UTF-8");
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();

		String loginId = user1.getLoginId();
		String Name = user1.getName();
		int Weight = user1.getWeight();
		//計算のための1日に必要なカロリーを入手
		User user = userDao.OneDayEnergy(loginId);
		int oneDayEnergy = user.getOneDayEnergy();
		//当日のカロリー消費の値を取り出す
		List<User> ExerciseList = userDao.findAllExercise(loginId);
		int exercisetotal=0;
		for(User exercise:ExerciseList) {
			exercisetotal+=exercise.getBurnEnergy();
			}
		//当日に食べた摂取物を取り出す
		List<User> FoodList = userDao.findAllFood(loginId);
		int foodtotal=0;
		for(User food:FoodList) {
			foodtotal+=food.getIntakeEnergy();
			}
		// 1日に必要なカロリーと摂取物,エクササイズの計算
		int oneDayEnergy1 =oneDayEnergy-exercisetotal+foodtotal;
		//TotalカロリーをUPdateする。
		userDao.DayEnergyUpDate(oneDayEnergy1,loginId);
		System.out.println(Weight);
		 // 1日分のカロリーを計算後データをSessionする。
		User userInfo = new User();
		userInfo.setLoginId(loginId);
		userInfo.setName(Name);
		userInfo.setWeight(Weight);
		userInfo.setOneDayEnergy(oneDayEnergy1);
		session.setAttribute("userInfo", userInfo);
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bmr.jsp");
		dispatcher.forward(request, response);
	}
}
