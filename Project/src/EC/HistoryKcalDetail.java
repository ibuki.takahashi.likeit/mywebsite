package EC;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class HistoryKcalDetail
 */
@WebServlet("/HistoryKcalDetail")
public class HistoryKcalDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HistoryKcalDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる
		HttpSession session = request.getSession();
		User user1 =(User) session.getAttribute("userInfo");
		if(user1==null) {
			response.sendRedirect("Login");
			return;
		}
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
        UserDao userDao = new UserDao();

		// リクエストパラメータの入力項目を取得

		User user =(User) session.getAttribute("userInfo");
        String loginId = user.getLoginId();
        String Date = request.getParameter("date");
        System.out.println(Date);
		//1日分の摂取物を全て取得
        List<User> FoodList = userDao.TotalFood(loginId,Date);

      //1日分のエクササイズデータを取得
        List<User> ExList = userDao.TotalEx(loginId,Date);

      //1日分のTotalKcalデータを取得
        User user2 = userDao.TotalKcalOneDay(loginId,Date);
        int e = user2.getTotalEnergy();
        System.out.println(e);
   	 // 履歴のデータをSessionする。
   		session.setAttribute("FoodList", FoodList);
   		session.setAttribute("ExList", ExList);
   		session.setAttribute("TotalKcal", user2);
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/historydetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {



				response.sendRedirect("UserHome");
			}
		}