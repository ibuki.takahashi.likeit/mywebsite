package EC;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class OneDayEnergy
 */
@WebServlet("/OneDayEnergyUpDate")
public class OneDayEnergyUpDate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public OneDayEnergyUpDate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/totalenergy2.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
        UserDao userDao = new UserDao();

		// sessionから前ページの全情報を入手
        HttpSession session = request.getSession();
		User user1 =(User) session.getAttribute("userInfo");
        String loginId = user1.getLoginId();
        String password = user1.getPassword();
        int myEnergy = user1.getMyEnergy();
        String Name = user1.getName();
        int height = user1.getHeight();
        int weight = user1.getWeight();
        String bodyLevel = request.getParameter("bodyLevel");

		double bodyLevel1 = Double.parseDouble(bodyLevel);
		double myEnergy1 = (double)myEnergy;
		int oneDayEnergy = (int)(-bodyLevel1 * myEnergy1);

		if(bodyLevel.equals("")) {

			request.setAttribute("errMsg", "あなたの活動レベルを入力してください。");


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/totalenergy2.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//UpDateデータを登録。
		userDao.UpDateAccount(Name,weight,height,myEnergy,oneDayEnergy,loginId);
		//体重のみを体重テーブルに登録。
		userDao.Weight(loginId,weight);
		//ユーザ情報を引き出す。
		User user2 = userDao.findByLoginInfo(loginId,password);
	// 基礎代謝BMIのデータを消去。
		session.removeAttribute("userInfo");
	 // 1日分のカロリー計算後のデータをSessionする。
		session.setAttribute("userInfo", user2);

	 response.sendRedirect("UserHome");

	}
}
