package EC;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class EditIB
 */
@WebServlet("/EditIB")
public class EditIB extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditIB() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user1 =(User) session.getAttribute("userInfo");
		if(user1==null) {
			response.sendRedirect("Login");
			return;
		}
		 request.setCharacterEncoding("UTF-8");
	        UserDao userDao = new UserDao();

			User user =(User) session.getAttribute("userInfo");
	        String loginId = user.getLoginId();
	        //当日分のFood,Exerciseを全て取得。
	        List<User> FoodList = userDao.AllOneDayFood(loginId);
	        System.out.println(FoodList);
	        List<User> ExList = userDao.AllOneDayEx(loginId);
	        System.out.println(ExList);

	        session.setAttribute("FoodList", FoodList);
	        session.setAttribute("ExList", ExList);

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/editib.jsp");
		dispatcher.forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");
	        UserDao userDao = new UserDao();

			// リクエストパラメータの入力項目を取得
	        HttpSession session = request.getSession();
			User user =(User) session.getAttribute("userInfo");
	        String loginId = user.getLoginId();
	        String idF = request.getParameter("intake_date");
	        String idE = request.getParameter("burn_date");
	        System.out.println(idF);
	        System.out.println(idE);
	        //選択したFoodデータを削除
	        userDao.DeleteFood(idF,loginId);
	      //選択したExerciseデータを削除
	        userDao.DeleteEx(idE,loginId);

	        response.sendRedirect("EditIB");
		}

	}

