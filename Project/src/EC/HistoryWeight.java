package EC;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;

/**
 * Servlet implementation class HistoryWeight
 */
@WebServlet("/HistoryWeight")
public class HistoryWeight extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//1ページに表示する履歴数
	final static int PAGE_MAX_ITEM_COUNT = 14;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HistoryWeight() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる
		HttpSession session = request.getSession();
		try {
		User user1 =(User) session.getAttribute("userInfo");
		if(user1==null) {
			response.sendRedirect("Login");
			return;
		}
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
        UserDao userDao = new UserDao();

		// リクエストパラメータの入力項目を取得

		User user =(User) session.getAttribute("userInfo");
        String loginId = user.getLoginId();

        //ペジネーション
        //表示ページ番号 未指定の場合 1ページ目を表示
  		int pageNum = Integer.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));

		//体重を全て取得
        List<User> WeightList = userDao.AllWeightPage(loginId, pageNum, PAGE_MAX_ITEM_COUNT);
        System.out.println(WeightList);

     // 検索ワードに対しての総ページ数を取得
		double itemCount = UserDao.getWeightCount(loginId);
		int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);
   	 // 履歴のデータをSessionする。
   		session.setAttribute("WeightList", WeightList);
   		//総アイテム数
		request.setAttribute("itemCount", (int) itemCount);
		// 総ページ数
		request.setAttribute("pageMax", pageMax);
		// 表示ページ
		request.setAttribute("pageNum", pageNum);
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/weightdetail.jsp");
		dispatcher.forward(request, response);
	}catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");

	}
}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {



				response.sendRedirect("UserHome");
			}
		}