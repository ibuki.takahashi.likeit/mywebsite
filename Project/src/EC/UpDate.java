package EC;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/UpDate")
public class UpDate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpDate() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる

	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
	dispatcher.forward(request, response);
}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

     // sessionから前ページの全情報を入手
        HttpSession session = request.getSession();
		User user1 =(User) session.getAttribute("userInfo");
        String loginId = user1.getLoginId();
		String Name = request.getParameter("name");
		String weight = request.getParameter("weight");
		String height = request.getParameter("height");
		int myEnergy=0;
		double weight1 = Double.parseDouble(weight);
		double height1 = Double.parseDouble(height);
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();

		/** テーブルに該当のデータが見つからなかった場合 **/
		if(Name.equals("")||weight.equals("")||height.equals("")) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。");


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
		}
		//年齢取得,性別取得
				User user = userDao.findByAge(loginId);
				int age = user.getAge();
				int gender =  user.getGender();
				String password = user.getPassword();
				System.out.println(gender);
				System.out.println(age);
		if(gender==0) {
			myEnergy= (int)((13.397*weight1)+(4.799*height1)-(5.677*age)+88.362);
		}

		else {
			myEnergy= (int)((9.247*weight1)+(3.098*height1)-(4.33*age)+447.593);

		}
		// SignUp時のデータを消去。
				session.removeAttribute("userInfo");
				// アカウント作成時のデータをSessionする。
				User userInfo = new User();
				userInfo.setLoginId(loginId);
				userInfo.setPassword(password);
				userInfo.setName(Name);
				userInfo.setMyEnergy(myEnergy);
				userInfo.setHeight((int)height1);
				userInfo.setWeight((int)weight1);

				session.setAttribute("userInfo", userInfo);

				response.sendRedirect("OneDayEnergyUpDate");

			}

	}

