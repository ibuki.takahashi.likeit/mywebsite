package EC;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/UserBodyProfile")
public class UserBodyProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserBodyProfile() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる


		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userbody.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
        UserDao userDao = new UserDao();

		// リクエストパラメータの入力項目を取得
        HttpSession session = request.getSession();
		User user1 =(User) session.getAttribute("userInfo");
        String loginId = user1.getLoginId();
        String Name = user1.getName();
        String password = user1.getPassword();
		String Gender = request.getParameter("gender");
		String age = request.getParameter("age");
		int g;
		if(Gender==null) {
			g = 1;
		}else {
			g = 0;
		}
		String height = request.getParameter("height");
		String weight = request.getParameter("weight");
		int myEnergy=0;
		double weight1 = Double.parseDouble(weight);
		double height1 = Double.parseDouble(height);
		int age1 = Integer.parseInt(age);

		 User user = userDao.findBySameLoginId(loginId);
		 if(user!=null){
				request.setAttribute("errMsg", "不正なアクセス。");


				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
				dispatcher.forward(request, response);
				return;
			}

		if(age.equals("")||height.equals("")||weight.equals("")) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。");


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}

		if(g==0) {
			myEnergy= (int)((13.397*weight1)+(4.799*height1)-(5.677*age1)+88.362);
		}

		else {
			myEnergy= (int)((9.247*weight1)+(3.098*height1)-(4.33*age1)+447.593);

		}

		// SignUp時のデータを消去。
		session.removeAttribute("userInfo");
		// アカウント作成時のデータをSessionする。
		User userInfo = new User();
		userInfo.setLoginId(loginId);
		userInfo.setName(Name);
		userInfo.setPassword(password);
		userInfo.setAge(age1);
		userInfo.setGender(g);
		userInfo.setMyEnergy(myEnergy);
		userInfo.setHeight((int)height1);
		userInfo.setWeight((int)weight1);



		HttpSession session1 = request.getSession();
		session1.setAttribute("userInfo", userInfo);

		response.sendRedirect("OneDayEnergy");

	}
	}

