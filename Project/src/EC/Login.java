package EC;


import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる

	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
	dispatcher.forward(request, response);
}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		UserDao userDao = new UserDao();
		//パスワードを暗号化
		String password1=userDao.password(password);

		User user = userDao.findByLoginInfo(loginId, password1);

		/** テーブルに該当のデータが見つからなかった場合 **/
		if (user == null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ログインに失敗しました。");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}
		String loginId1 = user.getLoginId();
		String Name1 = user.getName();
		int Weight = user.getWeight();
		//計算のための1日に必要なカロリーを入手
		User user1 = userDao.OneDayEnergy(loginId1);
		int oneDayEnergy = user1.getOneDayEnergy();
		//当日のカロリー消費の値を取り出す
		List<User> ExerciseList = userDao.findAllExercise(loginId1);
		int exercisetotal=0;
		for(User exercise:ExerciseList) {
			exercisetotal+=exercise.getBurnEnergy();
			}
		//当日に食べた摂取物を取り出す
		List<User> FoodList = userDao.findAllFood(loginId1);
		int foodtotal=0;
		for(User food:FoodList) {
			foodtotal+=food.getIntakeEnergy();
			}
		// 1日に必要なカロリーと摂取物,エクササイズの計算
		int oneDayEnergy1 =oneDayEnergy-exercisetotal+foodtotal;
		//当日にアクセスがあるかないか確認
		List<User> AccessList = userDao.findAllAccess(loginId1);
		int accesstotal=0;
		for(User access:AccessList) {
			accesstotal+=access.getId();
			}
		System.out.println(accesstotal);

		//ない場合はアクセス登録して当日分のDayEnergyを登録
		//ある場合はスルー
		if(accesstotal ==0) {
			userDao.Access(loginId1);
			userDao.DayEnergyAdd(loginId1,oneDayEnergy1);
		}
		/** テーブルに該当のデータが見つかった場合 **/
		System.out.println(Weight);
		 // 1日分のカロリーを計算後データをSessionする。
		HttpSession session = request.getSession();
		User userInfo = new User();
		userInfo.setLoginId(loginId1);
		userInfo.setName(Name1);
		userInfo.setWeight(Weight);
		userInfo.setOneDayEnergy(oneDayEnergy1);
		session.setAttribute("userInfo", userInfo);
		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserHome");

	}

}
