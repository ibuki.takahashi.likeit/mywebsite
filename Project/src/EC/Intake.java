package EC;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import dao.UserDao;


/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/Intake")
public class Intake extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Intake() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// TODO 未実装：ログインセッションがある場合、ユーザ一覧画面にリダイレクトさせる
		HttpSession session = request.getSession();
		User user1 =(User) session.getAttribute("userInfo");
		if(user1==null) {
			response.sendRedirect("Login");
			return;
		}
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/intake.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");
        UserDao userDao = new UserDao();

		// リクエストパラメータの入力項目を取得
        HttpSession session = request.getSession();
		User user1 =(User) session.getAttribute("userInfo");
        String loginId = user1.getLoginId();
        String Name = user1.getName();
		String foodName = request.getParameter("foodName");
		String foodEnergy = request.getParameter("foodEnergy");

		if(foodName.equals("")||foodEnergy.equals("")) {

			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/intake.jsp");
			dispatcher.forward(request, response);
			return;
		}
		//摂取物登録
		userDao.Intake(loginId, foodName,foodEnergy);
		//計算のための1日に必要なカロリーを入手
		User user = userDao.OneDayEnergy(loginId);
		int oneDayEnergy = user.getOneDayEnergy();
		//当日に食べた摂取物を取り出す
		List<User> FoodList = userDao.findAllFood(loginId);
		int foodtotal=0;
		for(User food:FoodList) {
			foodtotal+=food.getIntakeEnergy();
			}
		//当日のカロリー消費の値を取り出す
		List<User> ExerciseList = userDao.findAllExercise(loginId);
		int exercisetotal=0;
		for(User exercise:ExerciseList) {
			exercisetotal+=exercise.getBurnEnergy();
			}
		// 1日に必要なカロリーと摂取物,エクササイズの計算
		int oneDayEnergy1 =oneDayEnergy+foodtotal-exercisetotal;


		// 基礎代謝BMIのデータを消去。
				session.removeAttribute("userInfo");
			 // 1日分のカロリーを再計算後データをSessionする。
				User userInfo = new User();
				userInfo.setLoginId(loginId);
				userInfo.setName(Name);
				userInfo.setOneDayEnergy(oneDayEnergy1);
				session.setAttribute("userInfo", userInfo);



		response.sendRedirect("UserHome");
	}
}