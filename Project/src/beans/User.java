package beans;

import java.io.Serializable;
import java.sql.Date;


public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private int age;
	private String password;
	private int weight;
	private int height;
	private int myEnergy;
	private int oneDayEnergy;
	private int gender;
	private int intakeEnergy;
	private int burnEnergy;
	private Date date;
	private int totalEnergy;
	private String intakeName;
	private String burnName;


	public User() {

	}
	public User(int oneDayEnergy) {
		this.oneDayEnergy = oneDayEnergy;
	}


	public User(String loginId, int weight) {
		this.loginId = loginId;
		this.weight = weight;
	}

	public User(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}

	public User(String loginId, String name, int myEnergy) {
		this.loginId = loginId;
		this.name = name;
		this.myEnergy = myEnergy;
	}

	public User(String loginId) {
		this.loginId = loginId;

	}
	public User(int totalEnergy,Date date) {
		this.totalEnergy = totalEnergy;
		this.date = date;

	}
	public User(int age,int gender) {
		this.age = age;
		this.gender = gender;

	}

	public User(int age,int gender,String password) {
		this.age = age;
		this.gender = gender;
		this.password = password;
	}

	public User(int weight, String loginId, String name, int oneDayEnergy) {
		this.weight = weight;
		this.loginId = loginId;
		this.name = name;
		this.oneDayEnergy = oneDayEnergy;

	}

	// 全てのデータをセットするコンストラクタ
	public User(int id, String loginId, String name, int age, String password, int weight,
			int height) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.age = age;
		this.password = password;
		this.weight = weight;
		this.height = height;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getMyEnergy() {
		return myEnergy;
	}
	public void setMyEnergy(int myEnergy) {
		this.myEnergy = myEnergy;
	}
	public int getOneDayEnergy() {
		return oneDayEnergy;
	}
	public void setOneDayEnergy(int oneDayEnergy) {
		this.oneDayEnergy = oneDayEnergy;
	}


	public int getGender() {
		return gender;
	}


	public void setGender(int gender) {
		this.gender = gender;
	}


	public int getIntakeEnergy() {
		return intakeEnergy;
	}


	public void setIntakeEnergy(int intakeEnergy) {
		this.intakeEnergy = intakeEnergy;
	}
	public int getBurnEnergy() {
		return burnEnergy;
	}
	public void setBurnEnergy(int burnEnergy) {
		this.burnEnergy = burnEnergy;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getTotalEnergy() {
		return totalEnergy;
	}
	public void setTotalEnergy(int totalEnergy) {
		this.totalEnergy = totalEnergy;
	}
	public String getIntakeName() {
		return intakeName;
	}
	public void setIntakeName(String intakeName) {
		this.intakeName = intakeName;
	}
	public String getBurnName() {
		return burnName;
	}
	public void setBurnName(String burnName) {
		this.burnName = burnName;
	}



}
