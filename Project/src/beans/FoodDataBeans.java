package beans;

import java.io.Serializable;


public class FoodDataBeans implements Serializable {
	private int id;
	private String name;
	private String detail;
	private int kcal;
	private String fileName;


	public int getId() {
		return id;
	}
	public void setId(int FoodId) {
		this.id = FoodId;
	}
	public String getName() {
		return name;
	}
	public void setName(String FoodName) {
		this.name = FoodName;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getKcal() {
		return kcal;
	}
	public void setPrice(int FoodKcal) {
		this.kcal = FoodKcal;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String filename) {
		this.fileName = filename;
	}

}