package beans;

import java.io.Serializable;


public class ExerciseDataBeans implements Serializable {
	private int id;
	private String name;
	private String detail;
	private int kcal;
	private String fileName;


	public int getId() {
		return id;
	}
	public void setId(int ExerciseId) {
		this.id = ExerciseId;
	}
	public String getName() {
		return name;
	}
	public void setName(String ExerciseName) {
		this.name = ExerciseName;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getKcal() {
		return kcal;
	}
	public void setPrice(int ExerciseKcal) {
		this.kcal = ExerciseKcal;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String filename) {
		this.fileName = filename;
	}

}