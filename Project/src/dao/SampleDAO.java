package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.ExerciseDataBeans;
import beans.FoodDataBeans;


/**
 *
 * @author d-yamaguchi
 *
 */
public class SampleDAO {



	/**
	 * ランダムで引数指定分のFoodDataBeansを取得
	 * @param limit 取得したいかず
	 * @return <FoodDataBeans>
	 * @throws SQLException
	 */
	public static ArrayList<FoodDataBeans> getRandFood(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM Food ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<FoodDataBeans> itemList = new ArrayList<FoodDataBeans>();

			while (rs.next()) {
				FoodDataBeans item = new FoodDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("kcal"));
				item.setFileName(rs.getString("file_name"));
				itemList.add(item);
			}
			System.out.println("getAllItem completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	/**
	 * ランダムで引数指定分のExerciseDataBeansを取得
	 * @param limit 取得したいかず
	 * @return <ExerciseDataBeans>
	 * @throws SQLException
	 */
	public static ArrayList<ExerciseDataBeans> getRandExercise(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM Exercise ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<ExerciseDataBeans> itemList = new ArrayList<ExerciseDataBeans>();

			while (rs.next()) {
				ExerciseDataBeans item = new ExerciseDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("kcal"));
				item.setFileName(rs.getString("file_name"));
				itemList.add(item);
			}
			System.out.println("getAllItem completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
