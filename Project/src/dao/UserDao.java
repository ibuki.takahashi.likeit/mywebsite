package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.User;




public class UserDao {


	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM User WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int Weight = rs.getInt("weight");
			String loginId1 = rs.getString("login_id");
			String name = rs.getString("user_name");
			int oneDayEnergy = rs.getInt("oneday_energy");

			return new User(Weight,loginId1, name, oneDayEnergy);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public void Exercise(String loginId, String exName, String exEnergy) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO Burn(userid,burn_name,burn_energy,date)VALUES (?,?,?,NOW())";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, exName);
			pStmt.setString(3, exEnergy);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void Intake(String loginId, String foodName, String foodEnergy) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO Intake(userid,intake_name,intake_energy,date)VALUES (?,?,?,NOW())";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, foodName);
			pStmt.setString(3, foodEnergy);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public User findBySameLoginId(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	//パスワード暗号化
	public String password(String password) {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";
		String result=null;
		//ハッシュ生成処理
		byte[] bytes;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

			result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return result;

		}

	public void Createaccount(String loginId, String password, String Name, int age, int weight, int height, int gender, int myEnergy, int oneDayEnergy) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO User(login_id,password,user_name,age,weight,height,gender,user_energy,oneday_energy)VALUES (?,?,?,?,?,?,?,?,?)";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			pStmt.setString(3, Name);
			pStmt.setInt(4, age);
			pStmt.setInt(5, weight);
			pStmt.setInt(6, height);
			pStmt.setInt(7, gender);
			pStmt.setInt(8, myEnergy);
			pStmt.setInt(9, oneDayEnergy);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void Weight(String loginId,int weight) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO Weight(userid,weight,date)VALUES (?,?,now())";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setInt(2, weight);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	//1日に必要なカロリーを入手
	public User OneDayEnergy(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM User WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int oneDayEnergy = rs.getInt("oneday_energy");

			return new User(oneDayEnergy);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public List<User> findAllFood(String loginId) {
		Connection conn = null;
		List<User> FoodList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM Intake where userid = '"+loginId+"' AND date = date(now());";

			// SELECTを実行し、結果表を取得
						Statement stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery(sql);

						// 結果表に格納されたレコードの内容を
						// Userインスタンスに設定し、ArrayListインスタンスに追加
						while (rs.next()) {
							User user = new User();
							 user.setIntakeEnergy(rs.getInt("intake_energy"));;


							FoodList.add(user);
						}
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						}
					}
					return FoodList;
				}
	public List<User> findAllExercise(String loginId) {
		Connection conn = null;
		List<User> ExerciseList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM Burn where userid = '"+loginId+"' AND date = date(now());";

			// SELECTを実行し、結果表を取得
						Statement stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery(sql);

						// 結果表に格納されたレコードの内容を
						// Userインスタンスに設定し、ArrayListインスタンスに追加
						while (rs.next()) {
							User user = new User();
							 user.setBurnEnergy(rs.getInt("burn_energy"));;


							ExerciseList.add(user);
						}
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						}
					}
					return ExerciseList;
				}
	public void Update(String loginId, String exName, String exEnergy) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "INSERT INTO Burn(userid,burn_name,burn_energy,date)VALUES (?,?,?,NOW())";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, exName);
			pStmt.setString(3, exEnergy);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public User findByAge(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM User WHERE login_id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String password = rs.getString("password");
			int age = rs.getInt("age");
			int gender = rs.getInt("gender");

			return new User(age,gender,password);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public void UpDateAccount(String Name, int weight, int height, int myEnergy, int oneDayEnergy, String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "UPDATE User SET user_name = ?,weight = ?,height = ?,user_energy = ?,oneday_energy = ? WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, Name);
			pStmt.setInt(2, weight);
			pStmt.setInt(3, height);
			pStmt.setInt(4, myEnergy);
			pStmt.setInt(5, oneDayEnergy);
			pStmt.setString(6, loginId);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

//1日分の摂取物を取得する。
public List<User> TotalFood(String loginId, String Date) {
	Connection conn = null;
	List<User> FoodList = new ArrayList<User>();

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM Intake where userid = '"+loginId+"' AND date = '"+Date+"';";

		// SELECTを実行し、結果表を取得
					Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(sql);

					// 結果表に格納されたレコードの内容を
					// Userインスタンスに設定し、ArrayListインスタンスに追加
					while (rs.next()) {
						User user = new User();
						 user.setIntakeName(rs.getString("intake_name"));;
						 user.setIntakeEnergy(rs.getInt("intake_energy"));;
						 user.setDate(rs.getDate("date"));;

						 FoodList.add(user);
					}
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				} finally {
					// データベース切断
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}
					}
				}
				return FoodList;
			}
//1日分のエクササイズデータを取得する。
public List<User> TotalEx(String loginId, String Date) {
	Connection conn = null;
	List<User> ExList = new ArrayList<User>();

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM Burn where userid = '"+loginId+"' AND date = '"+Date+"';";

		// SELECTを実行し、結果表を取得
					Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(sql);

					// 結果表に格納されたレコードの内容を
					// Userインスタンスに設定し、ArrayListインスタンスに追加
					while (rs.next()) {
						User user = new User();
						 user.setBurnName(rs.getString("burn_name"));
						 user.setBurnEnergy(rs.getInt("burn_energy"));
						 user.setDate(rs.getDate("date"));

						 ExList.add(user);
					}
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				} finally {
					// データベース切断
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}
					}
				}
				return ExList;
			}
public User TotalKcalOneDay(String loginId, String Date) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM Day_energy WHERE userid = ? AND date=?";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, loginId);
		pStmt.setString(2, Date);
		ResultSet rs = pStmt.executeQuery();

		// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
		if (!rs.next()) {
			return null;
		}

		// 必要なデータのみインスタンスのフィールドに追加
		int totalEnergy = rs.getInt("total_energy");
		Date date = rs.getDate("date");

		return new User(totalEnergy,date);

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
public List<User> AllWeight(String loginId) {
	Connection conn = null;
	List<User> WeightList = new ArrayList<User>();

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM Weight where userid = '"+loginId+"' ";

		// SELECTを実行し、結果表を取得
					Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(sql);

					// 結果表に格納されたレコードの内容を
					// Userインスタンスに設定し、ArrayListインスタンスに追加
					while (rs.next()) {
						User user = new User();
						 user.setWeight(rs.getInt("weight"));
						 user.setDate(rs.getDate("date"));
						 WeightList.add(user);
					}
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				} finally {
					// データベース切断
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}
					}
				}
				return WeightList;
			}
public void Delete(String loginId) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "DELETE FROM User where login_id = ?";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, loginId);
		pStmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
public List<User> AllOneDayFood(String loginId) {
	Connection conn = null;
	List<User> FoodList = new ArrayList<User>();

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM Intake where userid = '"+loginId+"' AND date = date(now())";

		// SELECTを実行し、結果表を取得
					Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(sql);

					// 結果表に格納されたレコードの内容を
					// Userインスタンスに設定し、ArrayListインスタンスに追加
					while (rs.next()) {
						User user = new User();
						user.setId(rs.getInt("intake_id"));;
						 user.setIntakeName(rs.getString("intake_name"));;
						 user.setIntakeEnergy(rs.getInt("intake_energy"));;

						 FoodList.add(user);
					}
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				} finally {
					// データベース切断
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}
					}
				}
				return FoodList;
			}
public List<User> AllOneDayEx(String loginId) {
	Connection conn = null;
	List<User> ExList = new ArrayList<User>();

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM Burn where userid = '"+loginId+"' AND date = date(now())";

		// SELECTを実行し、結果表を取得
					Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(sql);

					// 結果表に格納されたレコードの内容を
					// Userインスタンスに設定し、ArrayListインスタンスに追加
					while (rs.next()) {
						User user = new User();
						user.setId(rs.getInt("burn_id"));;
						 user.setBurnName(rs.getString("burn_name"));;
						 user.setBurnEnergy(rs.getInt("burn_energy"));;

						 ExList.add(user);
					}
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				} finally {
					// データベース切断
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}
					}
				}
				return ExList;
			}
public void DeleteFood(String idF,String loginId) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "DELETE FROM Intake where intake_id = ? And userid = ?";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, idF);
		pStmt.setString(2, loginId);
		pStmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
public void DeleteEx(String idE,String loginId) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "DELETE FROM Burn where burn_id = ? And userid = ?";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, idE);
		pStmt.setString(2, loginId);
		pStmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
public void Access(String loginId1) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "INSERT INTO Access(login_id,date)VALUES (?,NOW())";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, loginId1);
		pStmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
public List<User> findAllAccess(String loginId) {
	Connection conn = null;
	List<User> AccessList = new ArrayList<User>();

	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM Access Where login_id = '"+loginId+"' And date = date(now());";

		// SELECTを実行し、結果表を取得
					Statement stmt = conn.createStatement();
					ResultSet rs = stmt.executeQuery(sql);

					// 結果表に格納されたレコードの内容を
					// Userインスタンスに設定し、ArrayListインスタンスに追加
					while (rs.next()) {
						User user = new User();
						 user.setId(rs.getInt("id"));;


						 AccessList.add(user);
					}
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				} finally {
					// データベース切断
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							return null;
						}
					}
				}
				return AccessList;
			}
public void DayEnergyAdd(String loginId1,int oneDayEnergy1) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "INSERT INTO Day_energy(userid,total_energy,date)VALUES (?,?,NOW())";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, loginId1);
		pStmt.setInt(2, oneDayEnergy1);
		pStmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
public User findAccess(String loginId1) {
	Connection conn = null;
	try {
		// データベースへ接続
		conn = DBManager.getConnection();

		// SELECT文を準備
		String sql = "SELECT * FROM Access Where login_id = ? And date = date(now()); ";

		// SELECTを実行し、結果表を取得
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, loginId1);

		ResultSet rs = pStmt.executeQuery();

		// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
		if (!rs.next()) {
			return null;
		}

		// 必要なデータのみインスタンスのフィールドに追加
		String loginId = rs.getString("login_id");

		return new User(loginId);

	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		// データベース切断
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
}
	public void DayEnergyUpDate(int oneDayEnergy,String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "UPDATE Day_energy SET total_energy = ? WHERE userid = ? AND date = date(now());";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, oneDayEnergy);
			pStmt.setString(2, loginId);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
//ページネーションカウントトータルカロリー
	public static double getDayCount(String loginId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("select count(*) as cnt from Day_energy where userid =?");
			st.setString(1, loginId);
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
	//全ての日付の履歴を取得
public List<User> TotalHistoryOrigin(String loginId) {
		Connection conn = null;
		List<User> HistoryList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM Day_energy where userid = '"+loginId+"' ORDER BY energy_id DESC;";

			// SELECTを実行し、結果表を取得
						Statement stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery(sql);

						// 結果表に格納されたレコードの内容を
						// Userインスタンスに設定し、ArrayListインスタンスに追加
						while (rs.next()) {
							User user = new User();
							 user.setTotalEnergy(rs.getInt("total_energy"));;
							 user.setDate(rs.getDate("date"));;

							 HistoryList.add(user);
						}
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					} finally {
						// データベース切断
						if (conn != null) {
							try {
								conn.close();
							} catch (SQLException e) {
								e.printStackTrace();
								return null;
							}
						}
					}
					return HistoryList;
				}
//全ての日付の履歴を取得ページネーション
	public  List<User> TotalHistory(String loginId, int pageNum, int pageMaxItemCount) throws SQLException {
	Connection con = null;
	PreparedStatement st = null;
	try {

		int startiItemNum = (pageNum - 1) * pageMaxItemCount;
		con = DBManager.getConnection();
			// 全検索;
			st = con.prepareStatement("SELECT * FROM Day_energy WHERE userid = '"+loginId+"' ORDER BY energy_id  DESC LIMIT ?,?");
			st.setInt(1, startiItemNum);
			st.setInt(2, pageMaxItemCount);


		ResultSet rs = st.executeQuery();
		List<User> HistoryList = new ArrayList<User>();

		while (rs.next()) {
			User user = new User();
			user.setTotalEnergy(rs.getInt("total_energy"));;
			 user.setDate(rs.getDate("date"));;

			 HistoryList.add(user);
		}
		return HistoryList;

	} catch (SQLException e) {
		System.out.println(e.getMessage());
		throw new SQLException(e);
	} finally {
		if (con != null) {
			con.close();
		}
	}
}
	//ページネーションカウント体重
		public static double getWeightCount(String loginId) throws SQLException {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				st = con.prepareStatement("select count(*) as cnt from Weight where userid =?");
				st.setString(1, loginId);
				ResultSet rs = st.executeQuery();
				double coung = 0.0;
				while (rs.next()) {
					coung = Double.parseDouble(rs.getString("cnt"));
				}
				return coung;
			} catch (Exception e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			} finally {
				if (con != null) {
					con.close();
				}
			}
		}
		//全ての日付のweight履歴を取得ページネーション
		public  List<User> AllWeightPage(String loginId, int pageNum, int pageMaxItemCount) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {

			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();
				// 全検索;
				st = con.prepareStatement("SELECT * FROM Weight WHERE userid = '"+loginId+"' ORDER BY weight_id  DESC LIMIT ?,?");
				st.setInt(1, startiItemNum);
				st.setInt(2, pageMaxItemCount);


			ResultSet rs = st.executeQuery();
			List<User> WeightList = new ArrayList<User>();

			while (rs.next()) {
				User user = new User();
				 user.setWeight(rs.getInt("weight"));
				 user.setDate(rs.getDate("date"));

				 WeightList.add(user);
			}
			return WeightList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}

